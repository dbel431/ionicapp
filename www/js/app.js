// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});
angular.module('starter')
.service('dataBase', function() {
  var data=[];
    var db={
/*      get:function(key){
        return db.data[key];
      },*/
      set:function(bdata){
        data.push(angular.copy(bdata.value));
        //push(angular.copy(mainData.data1));
      },
      getAll:function(){
        //return  db.data;
        return data;
      }
    };
    return db;
  });
angular.module('starter')
.controller('mainCtrl', function($scope,$state,$ionicSlideBoxDelegate,dataBase) 
{
      $scope.data1={};
    var cntr=0;
    var temp={};
    var mainData=this;
    mainData.myForm1={};
    mainData.myForm2={};
    mainData.myForm3={};
    mainData.init=init;
    mainData.show1=show1;
    mainData.show2=show2;
    mainData.title='Package';
    mainData.acive1={'color':'red','font-size':'30px'};
    mainData.acive2={'color':'black','font-size':'30px'};
    mainData.data1=[];
    mainData.db=[];
    mainData.show1Cal=show1Cal;
    mainData.show2Cal=show2Cal;
/*    mainData.show3Cal=show3Cal;*/

    mainData.previous =previous;
    mainData.next=next;  
    mainData.slideIndex=1;
    mainData.slideChanged =slideChanged;

    mainData.dataBase={};

    function init(data)
    {
        alert('123');
    }
   
    function slideChanged(index) {
    mainData.slideIndex = index;
    console.log(index);
    };


      function next() {
         $ionicSlideBoxDelegate.next();
      };
      function previous() {
        $ionicSlideBoxDelegate.previous();
      };
    function show1Cal(d1)
    {

      /**/
      mainData.myForm1.name=!d1.name.$valid;
      mainData.myForm1.width=!d1.width.$valid;
      mainData.myForm1.blength=!d1.blength.$valid;
      mainData.myForm1.height=!d1.height.$valid;
      mainData.myForm1.weight=!d1.weight.$valid;
      /**/
      console.log(d1.$valid);
       console.log(d1.name.$valid);
       if(!d1.$valid)
       {
        return;
       }

        $scope.data1.shap='bottle';
    /*   mainData.data1.push(angular.copy($scope.data1));
        console.log(mainData.data1);*/
        dataBase.set({key:cntr,value:angular.copy($scope.data1)});
        console.log(dataBase.getAll());
        cntr+=1;
        $scope.data1={};
    }
      function show2Cal(d1)
    {
      /**/
      mainData.myForm2.name=!d1.name.$valid;
      mainData.myForm2.width=!d1.width.$valid;
      mainData.myForm2.length=!d1.length.$valid;
      mainData.myForm2.height=!d1.height.$valid;
      mainData.myForm2.weight=!d1.weight.$valid;
      /**/
       if(!d1.$valid)
       {
        return;
       }

        $scope.data1.shap='rectangle';
       dataBase.set({key:cntr,value:angular.copy($scope.data1)});
        console.log(dataBase.getAll());
        cntr+=1;
        $scope.data1={};
    }
/*     function show3Cal(d1)
    {
      mainData.myForm3.width=!d1.width.$valid;
      mainData.myForm3.length=!d1.length.$valid;
      mainData.myForm3.height=!d1.height.$valid;

      console.log(d1.$valid);
       console.log(d1.name.$valid);
       if(!d1.$valid)
       {
        return;
       }
      mainData.data3.shap='Pallet';
      dataBase.set(mainData.data3);
    }*/
    function show1()
    {
      console.log("show1");
      mainData.acive2={'color':'black','font-size':'30px'};
      mainData.acive1={'color':'red','font-size':'30px'};
      $state.go('starter.show1');
      mainData.title='Package';
    }
     function show2()
    {
      mainData.acive2={'color':'red','font-size':'30px'};
      mainData.acive1={'color':'black','font-size':'30px'};
      console.log("show2");
      $state.go('starter.show2');
      mainData.title='Pallets';
   console.log(dataBase.getAll());
   $scope.data1=  dataBase.getAll();
    }
});
angular.module('starter')
.controller('palletsCtrl', function($scope,$state,$ionicSlideBoxDelegate,dataBase) 
{
    console.log('palletsCtrl');
    var mainData=this;
    mainData.db=dataBase.getAll();
    console.log(mainData.db);
    mainData.showById=showById;
    function showById(data)
    {
      console.log(data);
    }

});
angular.module('starter')
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('starter', {
          url: '/main',
          templateUrl: 'app/main/main.html',
          controller: 'mainCtrl as mainData'
        })
        .state('starter.show1', {
          url: '/show1',
          views: {
           'home-tab': {
              templateUrl: 'app/show1/show1.html',
              controller: 'mainCtrl as mainData'
            }
          }
        })
        .state('starter.show2', {
          url: '/show2',
          views: {
           'home-tab': {
              templateUrl: 'app/show2/show2.html',
              controller: 'palletsCtrl as mainData'
            }
          }
        })
        ;
      $urlRouterProvider.otherwise('/main/show1');
});
